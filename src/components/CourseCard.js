
import { Row, Col } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useState, useContext, useEffect } from 'react';

import UserContext from '../UserContext.js'
import {Link} from 'react-router-dom';


export default function Course({ courseProp }) {
  const { _id, name, description, price } = courseProp;

  // Use the state hook for this component to be able to store its state.
  // States are used to keep track of information related to individual components.
    /*
      Syntax:
        const [getter, setter] = useState(initialGetterValue);

        - getter will be our variable, while setter will be a function that will change the value of our getter
        - initialGetterValue will contain the getter's initial value
    */


  //const [getter , setter ] = useStates(initialGetterValue)

  // Note that useStates are only accessible internally; used to maintain a local component.
  // useEffect is used to perform an operation after a render that may result in changes

  
  // Activity Enrollees Section
  const [enrollees, setEnrollees] = useState(0);
  const [seats, setSeats] = useState(30);
  const [isDisabled, setIsDisabled] = useState (false)

  const {user} = useContext(UserContext)

 function enroll () {
    
    if(enrollees < 29 && seats > 1){
      setEnrollees(enrollees+1);
      setSeats(seats-1);
    } else {
      alert('Congratulations! You are the final enrollee.');
      setEnrollees(enrollees+1);
      setSeats(seats-1);
    }
  };

  // Define a useEffect hook to have the CourseCard component perform a certain task
  // This will run automatically.
    /*
      Syntax: 
        useEffect(sideEffect/function, [dependencies]);

        - sideEffect/function: runs on the first load and will reload depending on the dependency array
    */

  useEffect(()=> {
    if(seats === 0){
      setIsDisabled(true);
    }
  }, [seats]);





  return (


    <Row className = "mt-3">
      <Col>
        <Card>
            <Card.Body>
              <Card.Title>{name}:</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>

              <Card.Subtitle>Enrollees:</Card.Subtitle>
              <Card.Text>{enrollees}</Card.Text>
              
              <Card.Subtitle>Available Seats:</Card.Subtitle>
              <Card.Text>{seats}</Card.Text>
                            
                {
                  user ?
                  <Button as = {Link} to = {`/course/${_id}`} disabled = {isDisabled}>see more details</Button>
                  :
                  <Button as = {Link} to ="/login">Login</Button>
                }

              
            </Card.Body>
          </Card>
        </Col>
    </Row>
    )
};

