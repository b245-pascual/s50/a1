import {Link} from 'react-router-dom';
import {Container} from 'react-bootstrap'

export default function PageNotFound (){

	return(
		<Container className = "mt-3">
			<h1>404</h1>
			<h2>(Page Not Found.)</h2>
			<h3>Head back to <Link to = "/">home</Link></h3>
		</Container>
		)
}
