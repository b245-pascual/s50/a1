import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {Fragment} from 'react';
import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function Register(){

	// Create three useStates to store the values from the respective inputs of the email, password, and confirm password fields.

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPass, setConfirmPass] = useState("");

	// const [user, setUser] = useState(localStorage.getItem('email'));

	const {user, setUser} = useContext(UserContext);
	const navigate = useNavigate();

	// Create another state for the button.
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if(firstName !== "" && lastName !== "" && mobileNo !== "" && email !== "" && password !== "" && confirmPass !== "" && password === confirmPass){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, mobileNo, email, password, confirmPass])

	// Note that the useEffect's function or sideEffect will depend on the dependency that it is set to monitor in the [square brackets]. If the dependency bracket is empty, then the sideEffect will only run on the initial page load.

	function register (event) {
		event.preventDefault();
		
		fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo,
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);
			if(data){
				Swal.fire({
					title: 'You are now Registered. Thank you very much!',
					icon: 'success'
				});
				navigate('/login');
			} else {
				Swal.fire({
					title: 'Registration error!',
					text: 'Please try again',
					icon: 'error'
				});
			}
		})

		/*localStorage.setItem("email", email);
		setUser(localStorage.getItem('email'));
		alert('Congratulations! You are now registered with us.');
		setEmail("");
		setPassword("");
		setConfirmPass("");*/
	}

	return(
		user ?
		<Navigate to = "*"/>
		:
		<Fragment>
		<Container>
			<h1 className = "text-center mt-5">Register</h1>
			<Form className = "mt-5" onSubmit = {event => register(event)}>

			    <Container>
			    	<Row>
			    		<Col>
							<Form.Group className="mb-1" controlId="formBasicFirstName">
							  <Form.Label>First Name</Form.Label>
							  <Form.Control 
							  	type="string" 
							  	placeholder="Enter your first name" 
							  	value = {firstName}
							  	onChange = {event => setFirstName(event.target.value)}
							  	required
							  	/>
							</Form.Group>
						</Col>
						<Col>
							<Form.Group className="mb-1" controlId="formBasicLastName">
							  <Form.Label>Last Name</Form.Label>
							  <Form.Control 
							  	type="string" 
							  	placeholder="Enter your last name" 
							  	value = {lastName}
							  	onChange = {event => setLastName(event.target.value)}
							  	required
							  	/>
							</Form.Group>
						</Col>
					</Row>

					<Row className = "mb-3">
					<Form.Text className="text-muted">
					    We'll never share your personal data with anyone else.
					</Form.Text>
					</Row>

					<Form.Group className="mb-3" controlId="formBasicMobile">
					  <Form.Label>Mobile Number</Form.Label>
					  <Form.Control 
					  	type="string" 
					  	placeholder="987654321" 
					  	value = {mobileNo}
					  	onChange = {event => setMobileNo(event.target.value)}
					  	required
					  	/>
					</Form.Group>

			      <Form.Group className="mb-3" controlId="formBasicEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			        	type="email" 
			        	placeholder="Enter email" 
			        	value = {email}
			        	onChange = {event => setEmail(event.target.value)}
			        	required
			        	/>
			        <Form.Text className="text-muted">
			          We'll never share your email with anyone else.
			        </Form.Text>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="formBasicPassword">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Password"
			        	value = {password}
			        	onChange = {event => setPassword(event.target.value)}
			        	required
			        	/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="formConfirmPassword">
			        <Form.Label>Confirm Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Confirm your password"
			        	value = {confirmPass}
			        	onChange = {event => setConfirmPass(event.target.value)}
			        	required
			        	/>
			      </Form.Group>

			      {/*In this code block, we have an example of ternary operation and conditional rendering, which depends on the state of the variant isActive.*/}
			      {
			      	isActive ?
			      	<Button variant="primary" type="submit">
			      	  Submit
			      	</Button>
			      	:
			      	<Button variant="danger" type="submit" disabled>
			      	  Submit
			      	</Button>
			      }
			</Container>
		    </Form>
		</Container>
	    </Fragment>
		)
};