import './App.css';
import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';

import AppNavBar from './components/AppNavBar.js';
// import Banner from './Banner.js';
// import Highlights from './Highlights.js';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js'
import NotFound from './pages/NotFound.js'
import CourseView from './components/CourseView.js'
//import modules from react-routing-dom for the routing

import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';

//import UserProvider
import {UserProvider} from './UserContext.js';

function App() {
  const [user, setUser] = useState(null);

  useEffect(()=>{
    console.log(user);
  }, [user]);

  const unSetUser = ()=>{
    localStorage.clear();
  }

  //Storing information in a context object is done by providing the information using the correspoding "Provider" and passing information thru the prop value;
  //all infromation/data provided to the Provider component can be access later on from the context object properties
    
  return (
     <Router>
       <UserProvider value ={{user, setUser, unSetUser}}>
         
            <AppNavBar/>
            <Routes>
                <Route path = "*" element = {<NotFound/>}/>
                <Route path="/" element = {<Home/>} />
                <Route path = "/courses" element ={<Courses/>} />
                <Route path ="/login" element = {<Login/>}/>
                <Route path = "/register" element = {<Register/>}/>
                <Route path = "/logout" element = {<Logout/>}/>
                <Route path = "/course/:courseId" element = {<CourseView/>}/>
                
            </Routes>
         
      </UserProvider>
     </Router>
  );
}

export default App;
